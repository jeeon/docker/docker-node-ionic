#
# Install Ionic and other NPM packages
#

ENV CODE_PUSH_CLI_VERSION=2.1.9 \
    CORDOVA_VERSION=8.1.2 \
    IONIC_VERSION=3.20.1

RUN npm install --quiet --global --unsafe-perm \
        code-push-cli@$CODE_PUSH_CLI_VERSION \
        cordova@$CORDOVA_VERSION \
        ionic@$IONIC_VERSION && \
    \
    rm -rf ~/.npm
