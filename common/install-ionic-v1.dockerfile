#
# Install Ionic and other NPM packages
#

ENV CODE_PUSH_CLI_VERSION=2.1.9 \
    CORDOVA_VERSION=6.5.0 \
    IONIC_VERSION=2.2.3

RUN npm install --quiet --global --unsafe-perm \
        code-push-cli@$CODE_PUSH_CLI_VERSION \
        cordova@$CORDOVA_VERSION \
        ionic@$IONIC_VERSION && \
    \
    rm -rf ~/.npm
