#
# Use Jeeon's node-chrome base image
#

FROM jeeon/node-chrome:6-default

include(common/maintainer.dockerfile)

include(common/install-ionic-v1.dockerfile)
