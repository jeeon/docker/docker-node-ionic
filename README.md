# docker-node-ionic

Ready-made docker image for building and end-to-end testing of
**IonicJS Apps** using **Headless Chrome**.

Very specific versions (some non-latest) are used.

- node (using base image jeeon/node-chrome)
    - @ 6-default *(for ionic-1)*
    - @ 6-alpine *(for ionic-1)*
    - @ 8-default *(for ionic-3)*
    - @ 8-alpine *(for ionic-3)*
    - @ 10-default *(for ionic-3)* *(latest)*
    - @ 10-alpine *(for ionic-3)*
- npm
    - code-push-cli @ 2.1.9 *(prefer latest 2.x)*
    - cordova 
        - @ 6.5.0 *(for ionic-1, prefer latest 6.x)*
        - @ 8.1.2 *(for ionic-3, prefer latest 8.x)*
    - ionic 
        - @ 2.2.3 *(for ionic-1, prefer latest 2.x)*
        - @ 3.20.1 *(for ionic-3, prefer latest 3.x)*

### Pull from Docker Hub
```
docker pull jeeon/node-ionic:6-default
docker pull jeeon/node-ionic:6-alpine
docker pull jeeon/node-ionic:8-default
docker pull jeeon/node-ionic:8-alpine
docker pull jeeon/node-ionic:10-default
docker pull jeeon/node-ionic:10-alpine
```

### Build from GitLab
```
docker build -t jeeon/node-ionic:6-default gitlab.com/jeeon/docker/node-ionic/6-default
docker build -t jeeon/node-ionic:6-alpine gitlab.com/jeeon/docker/node-ionic/6-alpine
docker build -t jeeon/node-ionic:8-default gitlab.com/jeeon/docker/node-ionic/8-default
docker build -t jeeon/node-ionic:8-alpine gitlab.com/jeeon/docker/node-ionic/8-alpine
docker build -t jeeon/node-ionic:10-default gitlab.com/jeeon/docker/node-ionic/10-default
docker build -t jeeon/node-ionic:10-alpine gitlab.com/jeeon/docker/node-ionic/10-alpine
```

### Run image
```
docker run -it jeeon/node-ionic:6-default bash
docker run -it jeeon/node-ionic:6-alpine sh
docker run -it jeeon/node-ionic:8-default bash
docker run -it jeeon/node-ionic:8-alpine sh
docker run -it jeeon/node-ionic:10-default bash
docker run -it jeeon/node-ionic:10-alpine sh
```

### Use as base image
```Dockerfile
FROM jeeon/node-ionic:6-default
FROM jeeon/node-ionic:6-alpine
FROM jeeon/node-ionic:8-default
FROM jeeon/node-ionic:8-alpine
FROM jeeon/node-ionic:10-default
FROM jeeon/node-ionic:10-alpine
```

## License

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
