#
# Use Jeeon's node-chrome base image
#

FROM jeeon/node-chrome:10-alpine

include(common/maintainer.dockerfile)

include(common/install-ionic-v3.dockerfile)
