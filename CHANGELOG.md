# Changelog

All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](http://keepachangelog.com/en/1.0.0/)
and this project adheres to [Semantic Versioning](http://semver.org/spec/v2.0.0.html).

## 2019-03-06

### New

- Auto-trigger building dependant docker images.

## 2019-03-01

### New

- Add docker images for Node 10 (based on stretch and alpine).

### Changed

- Make code more DRY by using `m4` to generate `Dockerfile`s from common snippets.
- Upgrade all system packages.
- Upgrade cordova-cli to v8.1.2 and ionic-cli to v3.20.1.

## 2019-01-05

### Changed

- Fresh rebuild to get latest system updates and renew latest android sdk licences.

## 2018-10-13

### Changed

- Update code-push-cli to v2.1.9.

## 2018-05-11

### Changed

- Use jeeon/node-chrome (appropriate tag) as the base image.
- Remove npm cache folder after installing packages.

## 2018-04-04

### Changed

- Update docs about preferred dependency versions.

## 2018-04-01

### Changed

- Start keeping a ChangeLog.
- Update ionic-cli to v2.2.3 (for Ionic 1) and v3.20.0 (for Ionic 3).
- Update code-push-cli to v2.1.8.
